import React from 'react';
import Image from "next/image";
import Link from "next/link";
import {FilterSuggestion} from "../../types/search";

type List = {
    show: Boolean,
    data: any,
    limit: number
}

const suggestionList = ({show, data, limit}: List) => {
    const {filteredSuggestions}: any = data
    return (show && (<div className="search_result">
        {filteredSuggestions[0]?.map((product: FilterSuggestion, i: number) => {
            if (limit >= i) {
                return (
                    <div key={product.id} className="search_content">
                        <div className="search_image">
                            <Link href={`/article/${product.id}`}>
                                <a>
                                    <Image src={product.thumbnail} width={80} height={80} alt={product.title}/>
                                </a>
                            </Link>
                        </div>
                        <div className="search_info">
                            <Link href={`/article/${product.id}`}><a><h3 className="title">{product.title}</h3>
                            </a></Link>
                        </div>
                        <div className="price_info">
                            <p className="price">{`$ ${product.price}`}</p>
                        </div>
                    </div>)
            }
        })}
    </div>))
}
export default suggestionList