import React from 'react';
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import ImageLogo from "../../public/assets/Logo_ML@2x.png"
import Search from "./search";

const Header = () => {
    return (
        <>
            <Head>
                <title>Mercado Libre</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css"
                      integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w=="
                      crossOrigin="anonymous" referrerPolicy="no-referrer"/>
                <link rel="shortcut icon" href="/assets/favicon.ico" />

            </Head>
            <div className="header">
                <div className="container">
                <div className="logo">
                    <Link href='/' ><a><Image src={ImageLogo} width={60} height={40} alt="mercado-libre" /></a></Link>
                </div>
                <div style={{width: '70%'}}>
                    <Search/>
                </div>
                <div></div>
                </div>
            </div>
        </>
    );
}

export default Header