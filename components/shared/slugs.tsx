import React, {useEffect, useState} from 'react';
import {axiosURL} from "../../helpers/axios";
import {BreadCrumb} from "../../types/breadcrumb";

type category = {
    category: string,
}

type DataCategory = {
    id: string,
    name: string
}

export const Slugs = ({category}: category) => {

    const [getCategory, setCategory] = useState<DataCategory[]>([]);

    useEffect(()=>{
        if (category.length > 0) {
            axiosURL(`categories/${category}`).then((category:any) => {
                const {path_from_root}:BreadCrumb = category.data
                setCategory(path_from_root);
            }).catch((error) => console.log(error));
        }
    },[category])

    return (
        <div className="slugs-content">
            <ul className="slugs">
                {getCategory && (
                    getCategory.map((slug: DataCategory) => {
                        return (<li className="list-slug" key={slug.id}>
                            {slug.name}
                        </li>)
                    })
                )}
            </ul>
        </div>
    )
}

export default Slugs;