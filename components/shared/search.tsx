import React, {useEffect, useState, useRef} from 'react'
import {useRouter} from 'next/router'
import Suggestion from './suggestionList';
import {axiosURL} from './../../helpers/axios';
import {SearchProduct} from "../../types/search";

type typeSuggestion = {
    filteredSuggestions: SearchProduct[];
    showSuggestions:     boolean;
    userInput:           string;
}

const Search = () => {

    const router = useRouter();
    let timerRef:any = useRef(null);

    const state:typeSuggestion = {
        filteredSuggestions: [],
        showSuggestions: false,
        userInput: ""
    }

    const [txtSearch, set_txtSearch] = useState('')

    const [FilteredSuggestions, setFilteredSuggestions] = useState<typeSuggestion>(state);

    useEffect(()=>{
        if(!state.showSuggestions){
            return () => clearTimeout(timerRef.current);
        }
    },[state.showSuggestions])

    const onChangeSearch = (search: string) => {
        set_txtSearch(search);
        if (!search) return null;
        const data: SearchProduct[] = [];
        if (search.length > 5) {
            //Search data Base
            axiosURL(`sites/MLA/search?q=${search}`).then(
                item => {
                    const {results} = item.data
                    data.push(results);
                })
                .catch(error => console.log(error));

            timerRef.current = setTimeout(() => {
                setFilteredSuggestions({
                    filteredSuggestions: data,
                    showSuggestions: true,
                    userInput: search
                });
            }, 1000);

        }
        setFilteredSuggestions({
            ...FilteredSuggestions,
            showSuggestions: false,
        });
    }

    const onKeyDown = (e: any) => {
        const {filteredSuggestions} = FilteredSuggestions;
        if (e.keyCode === 13) {
            if (filteredSuggestions) {
                router.push({
                    pathname: '/search/[search]',
                    query: {search: txtSearch},
                })
            }
            clearTimeout(timerRef.current);
        }
    }

    const onBlur = () => {
        timerRef.current = setTimeout(() => {
            setFilteredSuggestions({
                filteredSuggestions: [],
                showSuggestions: false,
                userInput: ""
            })
        }, 1000)
    }

    const clickSearch = (txtSearch: string) =>{
        router.push({
            pathname: '/search/[search]',
            query: {search: txtSearch},
        })
    }

    return (
        <>
            <div className="form_search">
                <input type="text"
                       onBlur={() => onBlur()}
                       onKeyDown={(e) => onKeyDown(e)}
                       onChange={(e) => onChangeSearch(e.target.value)}
                       value={txtSearch}
                       placeholder="Nunca dejes de Buscar..."
                       className="input_search"/>
                <button className="btn_search" onClick={()=>clickSearch(txtSearch)}  >
                    <i className="search-icon"></i>
                </button>
            </div>
            <Suggestion show={FilteredSuggestions.showSuggestions} data={FilteredSuggestions} limit={5}/>
        </>
    )
}
export default Search