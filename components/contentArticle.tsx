import React, {useState} from 'react';
import AliceCarousel from 'react-alice-carousel';
import {axiosURL} from "../helpers/axios";
import Image from 'next/image'

const ContentArticle = ({id, gallery}: any) => {

    const [getArticle, setArticle] = useState('')

    axiosURL(`items/${id}/description`).then(description => {
            const {data} = description
            setArticle(data.plain_text);
    })

    const items = gallery.map((item: any) => <Image key={item.id} src={item.url} alt={item.size} width={500} height={500} />);

    const responsive = {
        0: { items: 1 },
        568: { items: 1 },
        1024: { items: 1 },
    };

    return (<div>
        <div style={{padding: '0px 20px'}}>
            <AliceCarousel
                infinite
                autoHeight
                mouseTracking
                items={items}
                disableButtonsControls={true}
                responsive={responsive}

            />
            <p style={{ overflowX: 'hidden', textAlign: 'justify' }}>
                {getArticle}
            </p>
        </div>
    </div>)
}
export default ContentArticle