import React from 'react';
import Link from "next/link";
import Image from "next/image";
import {FilterConsult} from "../types";

type List = {
    key: string;
    list: FilterConsult;
}

const ArticleList = (list: List) => {
    const {list:{id, thumbnail, title, price, seller_address}} = list;

    return (
    <div className="article-list">
        <div className="article-image">
                <Link href={`/article/${id}`}><a style={{cursor: 'pointer'}} >
                    <Image src={thumbnail} width={110} height={110} alt={title} /></a>
                </Link>
            </div>
            <div className="article-content">
                <div className="article-info">
                    <h2 style={{ color: 'rgb(53, 53, 53)'}}>{`$ ${price}`}</h2>
                    <Link href={`/article/${id}`}><a style={{color: '#3383fa', textDecoration: 'none'}}>{title}</a></Link>
                </div>
                <div className="article-city">
                    {seller_address.city.name}
                </div>
            </div>
        </div>)
}
export default ArticleList;