import React, {ReactNode} from 'react';
import Header from "./shared/header";

type Props = {
    children?: ReactNode
}
const Layout = ({children}: Props) => {
    return (
        <>
            <Header/>
            <main>
                {children}
            </main>
        </>

    )
}

export default Layout;