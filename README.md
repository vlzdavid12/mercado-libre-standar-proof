![alt text](https://gitlab.com/vlzdavid12/mercado-libre-standar-proof/-/raw/main/ScreenShot.png)

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Demo
[Link Project](https://mrl-proof.vercel.app)






