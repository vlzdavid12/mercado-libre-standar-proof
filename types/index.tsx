export interface FilterConsult {
    id:                  string;
    site_id:             string;
    title:               string;
    seller:              Seller;
    price:               number;
    prices:              Prices;
    sale_price:          null;
    currency_id:         string;
    available_quantity:  number;
    sold_quantity:       number;
    buying_mode:         string;
    listing_type_id:     string;
    stop_time:           Date;
    condition:           string;
    permalink:           string;
    thumbnail:           string;
    thumbnail_id:        string;
    accepts_mercadopago: boolean;
    installments:        Installments;
    address:             Address;
    shipping:            Shipping;
    seller_address:      SellerAddress;
    attributes:          Attribute[];
    original_price:      null;
    category_id:         string;
    official_store_id:   null;
    domain_id:           string;
    catalog_product_id:  null;
    tags:                string[];
    order_backend:       number;
    use_thumbnail_id:    boolean;
}

export interface Address {
    state_id:   string;
    state_name: string;
    city_id:    null;
    city_name:  string;
}

export interface Attribute {
    value_struct:         null;
    attribute_group_name: string;
    id:                   string;
    name:                 string;
    value_id:             null | string;
    value_name:           string;
    values:               Value[];
    attribute_group_id:   string;
    source:               number;
}

export interface Value {
    id:     null | string;
    name:   string;
    struct: null;
    source: number;
}

export interface Installments {
    quantity:    number;
    amount:      number;
    rate:        number;
    currency_id: string;
}

export interface Prices {
    id:                    string;
    prices:                Price[];
    presentation:          Presentation;
    payment_method_prices: any[];
    reference_prices:      ReferencePrice[];
    purchase_discounts:    any[];
}

export interface Presentation {
    display_currency: string;
}

export interface Price {
    id:                    string;
    type:                  string;
    conditions:            Conditions;
    amount:                number;
    regular_amount:        null;
    currency_id:           string;
    exchange_rate_context: string;
    metadata:              Metadata;
    last_updated:          Date;
}

export interface Conditions {
    context_restrictions: any[];
    start_time:           Date | null;
    end_time:             Date | null;
    eligible:             boolean;
}

export interface Metadata {
}

export interface ReferencePrice {
    id:                    string;
    type:                  string;
    conditions:            Conditions;
    amount:                number;
    currency_id:           string;
    exchange_rate_context: string;
    tags:                  any[];
    last_updated:          Date;
}

export interface Seller {
    id:                 number;
    permalink:          string;
    registration_date:  Date;
    car_dealer:         boolean;
    real_estate_agency: boolean;
    tags:               string[];
    eshop:              Eshop;
    seller_reputation:  SellerReputation;
}

export interface Eshop {
    nick_name:        string;
    eshop_rubro:      null;
    eshop_id:         number;
    eshop_locations:  any[];
    site_id:          string;
    eshop_logo_url:   string;
    eshop_status_id:  number;
    seller:           number;
    eshop_experience: number;
}

export interface SellerReputation {
    transactions:        Transactions;
    power_seller_status: string;
    metrics:             Metrics;
    level_id:            string;
}

export interface Metrics {
    claims:                Cancellations;
    delayed_handling_time: Cancellations;
    sales:                 Sales;
    cancellations:         Cancellations;
}

export interface Cancellations {
    rate:   number;
    value:  number;
    period: string;
}

export interface Sales {
    period:    string;
    completed: number;
}

export interface Transactions {
    total:     number;
    canceled:  number;
    period:    string;
    ratings:   Ratings;
    completed: number;
}

export interface Ratings {
    negative: number;
    positive: number;
    neutral:  number;
}

export interface SellerAddress {
    id:           string;
    comment:      string;
    address_line: string;
    zip_code:     string;
    country:      City;
    state:        City;
    city:         City;
    latitude:     string;
    longitude:    string;
}

export interface City {
    id:   null | string;
    name: string;
}

export interface Shipping {
    free_shipping: boolean;
    mode:          string;
    tags:          string[];
    logistic_type: string;
    store_pick_up: boolean;
}

export interface ArticleType {
    id:                               string;
    site_id:                          string;
    title:                            string;
    subtitle:                         null;
    seller_id:                        number;
    category_id:                      string;
    official_store_id:                number;
    price:                            number;
    base_price:                       number;
    original_price:                   null;
    currency_id:                      string;
    initial_quantity:                 number;
    available_quantity:               number;
    sold_quantity:                    number;
    sale_terms:                       Attribute[];
    buying_mode:                      string;
    listing_type_id:                  string;
    start_time:                       Date;
    stop_time:                        Date;
    condition:                        string;
    permalink:                        string;
    thumbnail_id:                     string;
    thumbnail:                        string;
    secure_thumbnail:                 string;
    pictures:                         Picture[];
    video_id:                         null;
    descriptions:                     Description[];
    accepts_mercadopago:              boolean;
    non_mercado_pago_payment_methods: any[];
    shipping:                         Shipping;
    international_delivery_mode:      string;
    seller_address:                   SellerAddress;
    seller_contact:                   null;
    location:                         Location;
    coverage_areas:                   any[];
    attributes:                       Attribute[];
    warnings:                         any[];
    listing_source:                   string;
    variations:                       Variation[];
    status:                           string;
    sub_status:                       any[];
    tags:                             string[];
    warranty:                         string;
    catalog_product_id:               null;
    domain_id:                        string;
    parent_item_id:                   null;
    differential_pricing:             null;
    deal_ids:                         any[];
    automatic_relist:                 boolean;
    date_created:                     Date;
    last_updated:                     Date;
    health:                           number;
    catalog_listing:                  boolean;
    channels:                         string[];
}

export interface Attribute {
    id:                    string;
    name:                  string;
    value_id:              null | string;
    value_name:            string;
    value_struct:          null;
    values:                Value[];
    attribute_group_id:   AttributeGroupName | string;
    attribute_group_name: AttributeGroupName | string;
}

export enum AttributeGroupID {
    Empty = "",
    Others = "OTHERS",
}

export enum AttributeGroupName {
    Empty = "",
    Otros = "Otros",
}


export interface Value {
    id:     null | string;
    name:   string;
    struct: null;
}

export interface Description {
    id: string;
}

export interface Location {
}

export interface Picture {
    id:         string;
    url:        string;
    secure_url: string;
    size:       string;
    max_size:   string;
    quality:    string;
}

export interface SellerAddress {
    city:            City;
    state:           City;
    country:         City;
    search_location: SearchLocation;
    id:              string;
}

export interface City {
    id:   string | null;
    name: string;
}

export interface SearchLocation {
    city:  City;
    state: City;
}

export interface Shipping {
    mode:          string;
    methods:       any[];
    tags:          string[];
    dimensions:    null;
    local_pick_up: boolean;
    free_shipping: boolean;
    logistic_type: string;
    store_pick_up: boolean;
}

export interface Variation {
    id:                     number;
    price:                  number;
    attribute_combinations: AttributeCombination[];
    available_quantity:     number;
    sold_quantity:          number;
    sale_terms:             any[];
    picture_ids:            string[];
    catalog_product_id:     null;
}

export interface AttributeCombination {
    id:           string;
    name:         string;
    value_id:     string;
    value_name:   string;
    value_struct: null;
    values:       Value[];
}
