export interface SearchProduct {
    id: string;
    site_id: string;
    title: string;
    seller: Seller;
    price: number;
    prices: Prices;
    sale_price: null;
    currency_id: string;
    available_quantity: number;
    sold_quantity: number;
    buying_mode: string;
    listing_type_id: string;
    stop_time: Date;
    condition: string;
    permalink: string;
    thumbnail: string;
    thumbnail_id: string;
    accepts_mercadopago: boolean;
    installments: Installments;
    address: Address;
    shipping: Shipping;
    seller_address: SellerAddress;
    attributes: Attribute[];
    original_price: null;
    category_id: string;
    official_store_id: null;
    domain_id: string;
    catalog_product_id: string;
    tags: string[];
    catalog_listing: boolean;
    use_thumbnail_id: boolean;
    order_backend: number;
}

export interface Address {
    state_id: string;
    state_name: string;
    city_id: string;
    city_name: string;
}

export interface Attribute {
    id: string;
    value_struct: Struct | null;
    values: Value[];
    name: string;
    value_id: string;
    value_name: string;
    attribute_group_id: string;
    attribute_group_name: string;
    source: number;
}

export interface Struct {
    number: number;
    unit: string;
}

export interface Value {
    struct: Struct | null;
    source: number;
    id: string;
    name: string;
}

export interface Installments {
    quantity: number;
    amount: number;
    rate: number;
    currency_id: string;
}

export interface Prices {
    id: string;
    prices: Price[];
    presentation: Presentation;
    payment_method_prices: any[];
    reference_prices: any[];
    purchase_discounts: any[];
}

export interface Presentation {
    display_currency: string;
}

export interface Price {
    id: string;
    type: string;
    conditions: Conditions;
    amount: number;
    regular_amount: null;
    currency_id: string;
    exchange_rate_context: string;
    metadata: Metadata;
    last_updated: Date;
}

export interface Conditions {
    context_restrictions: any[];
    start_time: null;
    end_time: null;
    eligible: boolean;
}

export interface Metadata {
}

export interface Seller {
    id: number;
    permalink: string;
    registration_date: Date;
    car_dealer: boolean;
    real_estate_agency: boolean;
    tags: string[];
    eshop: Eshop;
    seller_reputation: SellerReputation;
}

export interface Eshop {
    nick_name: string;
    eshop_rubro: null;
    eshop_id: number;
    eshop_locations: any[];
    site_id: string;
    eshop_logo_url: string;
    eshop_status_id: number;
    seller: number;
    eshop_experience: number;
}

export interface SellerReputation {
    transactions: Transactions;
    power_seller_status: string;
    metrics: Metrics;
    level_id: string;
}

export interface Metrics {
    claims: Cancellations;
    delayed_handling_time: Cancellations;
    sales: Sales;
    cancellations: Cancellations;
}

export interface Cancellations {
    rate: number;
    value: number;
    period: string;
}

export interface Sales {
    period: string;
    completed: number;
}

export interface Transactions {
    total: number;
    canceled: number;
    period: string;
    ratings: Ratings;
    completed: number;
}

export interface Ratings {
    negative: number;
    positive: number;
    neutral: number;
}

export interface SellerAddress {
    id: string;
    comment: string;
    address_line: string;
    zip_code: string;
    country: City;
    state: City;
    city: City;
    latitude: string;
    longitude: string;
}

export interface City {
    id: string;
    name: string;
}

export interface Shipping {
    free_shipping: boolean;
    mode: string;
    tags: string[];
    logistic_type: string;
    store_pick_up: boolean;
}

export interface FilterSuggestion {
    id: string;
    site_id: string;
    title: string;
    seller: Seller;
    price: number;
    prices: Prices;
    sale_price: null;
    currency_id: string;
    available_quantity: number;
    sold_quantity: number;
    buying_mode: string;
    listing_type_id: string;
    stop_time: Date;
    condition: string;
    permalink: string;
    thumbnail: string;
    thumbnail_id: string;
    accepts_mercadopago: boolean;
    installments: null;
    address: Address;
    shipping: Shipping;
    seller_address: SellerAddress;
    seller_contact: SellerContact;
    location: Location;
    attributes: Attribute[];
    original_price: null;
    category_id: string;
    official_store_id: null;
    domain_id: string;
    catalog_product_id: null;
    tags: string[];
    order_backend: number;
    use_thumbnail_id: boolean;
}

export interface Address {
    state_id: string;
    state_name: string;
    city_id: string;
    city_name: string;
    area_code: string;
    phone1: string;
}

export interface Attribute {
    value_id: string;
    value_name: string;
    values: Value[];
    source: number;
    id: string;
    name: string;
    value_struct: Struct | null;
    attribute_group_id: string;
    attribute_group_name: string;
}

export interface Struct {
    number: number;
    unit: string;
}

export interface Value {
    source: number;
    id: string;
    name: string;
    struct: Struct | null;
}

export interface Location {
    address_line: string;
    zip_code: string;
    subneighborhood: null;
    neighborhood: City;
    city: City;
    state: City;
    country: City;
    latitude: number;
    longitude: number;
}

export interface City {
    id: string;
    name: string;
}

export interface Prices {
    id: string;
    prices: Price[];
    presentation: Presentation;
    payment_method_prices: any[];
    reference_prices: any[];
    purchase_discounts: any[];
}

export interface Presentation {
    display_currency: string;
}

export interface Price {
    id: string;
    type: string;
    conditions: Conditions;
    amount: number;
    regular_amount: null;
    currency_id: string;
    exchange_rate_context: string;
    metadata: Metadata;
    last_updated: Date;
}

export interface Conditions {
    context_restrictions: any[];
    start_time: null;
    end_time: null;
    eligible: boolean;
}

export interface Metadata {
}

export interface Seller {
    id: number;
    permalink: string;
    registration_date: Date;
    car_dealer: boolean;
    real_estate_agency: boolean;
    tags: string[];
    seller_reputation: SellerReputation;
}

export interface SellerReputation {
    transactions: Transactions;
    power_seller_status: string;
    metrics: Metrics;
    level_id: string;
}

export interface Metrics {
    claims: Cancellations;
    delayed_handling_time: Cancellations;
    sales: Sales;
    cancellations: Cancellations;
}

export interface Cancellations {
    rate: number;
    value: number;
    period: string;
}

export interface Sales {
    period: string;
    completed: number;
}

export interface Transactions {
    total: number;
    canceled: number;
    period: string;
    ratings: Ratings;
    completed: number;
}

export interface Ratings {
    negative: number;
    positive: number;
    neutral: number;
}

export interface SellerAddress {
    id: string;
    comment: string;
    address_line: string;
    zip_code: string;
    country: City;
    state: City;
    city: City;
    latitude: string;
    longitude: string;
}

export interface SellerContact {
    contact: string;
    other_info: string;
    area_code: string;
    phone: string;
    area_code2: string;
    phone2: string;
    email: string;
    webpage: string;
}

export interface Shipping {
    free_shipping: boolean;
    mode: string;
    tags: string[];
    logistic_type: string;
    store_pick_up: boolean;
}