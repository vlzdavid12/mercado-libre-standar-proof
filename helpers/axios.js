import axios from 'axios';
export const axiosURL =  axios.create({
    baseURL: 'https://api.mercadolibre.com/',
})