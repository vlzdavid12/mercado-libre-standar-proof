import React from  'react';
import Layout from "../components/layout";
import Image from "next/image";
import LogoMCL from "../public/assets/logo_mercado_libre.png"

export default function Home() {
  return (
      <Layout>
          <div style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              margin: '120px 0px auto',
          }}>
          <Image src={LogoMCL} width={510} height={150} alt="mercado-libre" />
          </div>
          <p style={{textAlign:'center', color: '#4e4e4e', padding: '10px'}}>Ingresa el producto de tu interes.</p>
          <p style={{textAlign:'center', color: '#4e4e4e', padding: '10px'}}>Nota: Para aparecer el Auto Complete ingresa 6 carácteres como minimo  en el campo de la busqueda.
          </p>
          <h4 style={{textAlign: 'center'}}>Ejemplo: casco para moto</h4>
      </Layout>
  )
}
