import React, {useEffect, useState} from 'react';
import {useRouter} from "next/router";
import {axiosURL} from "../../helpers/axios";

import Layout from "../../components/layout";
import ContentArticle from "../../components/contentArticle";
import Slugs from "../../components/shared/slugs";

import {ArticleType, Picture} from "../../types";

const Article = () => {

    const router = useRouter();
    //Capture txt query search
    const {query: {article}} = router;

    const [getData, setData] = useState<ArticleType>();


    useEffect(() => {

        const searchArticle = (id: string | string[] | undefined) => {
            if (id) {
                try{
                axiosURL(`items/${id}`).then((article: any) => {
                    setData(article.data);
                }).catch((error) => {
                    console.error(error);
                    router.push('/404')
                })}catch (error){
                    console.error(error);
                    router.push('/')
                }
            }
        }

        searchArticle(article);

    }, [article, router])



    if (!getData) return null;

    const {pictures, category_id, condition ,sold_quantity, title, price} = getData

    //Gallery Pictures
    const photos = pictures.map((picture: Picture) => picture)
    return (
        <Layout>
            <div className="article-general">
                <Slugs category={category_id}/>
                <div className="article-info">
                    <div className="column-article container">
                        <ContentArticle id={article} gallery={photos}/>
                    </div>
                    <aside className="column-article aside-container">
                        <p>{condition.toUpperCase().charAt(0) + condition.slice(1)} - {sold_quantity} vendidos</p>
                        <h2 className="title">{title}</h2>
                        <h4 className="price">{`$ ${price}`}</h4>
                        <button className="btn-payment">Comprar</button>
                    </aside>
                </div>
            </div>
        </Layout>
    )
}
export default Article;