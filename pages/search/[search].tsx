import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router'
import Layout from "../../components/layout";
import {axiosURL} from "../../helpers/axios";
import ArticleList from "../../components/articleList";
import Slugs from "../../components/shared/slugs";
import {FilterConsult} from "../../types";


const Search = () => {
    const router = useRouter();

    const [getData, setData] = useState<FilterConsult[]>([]);
    const [getID, setID] = useState<string>('');

    const {query:{search}} = router;

    useEffect(()=>{
        const searchTXT = (product: string | string[] | undefined) => {
            try{
                axiosURL(`sites/MLA/search?q=${product}`)
                    .then((article) => {
                        const {data: {results}}:any= article;
                        setID(results[0].category_id)
                        setData(results);})
                    .catch((error)=> router.push('/404'))
            }catch (error){
                console.error(error);
                router.push('/');
            }

        }
        searchTXT(search);
    },[router, search])


    if(getData.length < 0) return <div>Not result..</div>;
    return (<Layout>
        <div className="bg-article">
            <Slugs category={getID} />
            <div className="container">
                {getData.map((item: FilterConsult)=> {
                    return <ArticleList key={item.id} list={item}/>
                })}
            </div>
        </div>
    </Layout>)
}
export default Search;